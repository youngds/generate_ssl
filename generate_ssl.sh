#/*************************************************
#*  generate_ssl.sh write by echo at Changsha. Hunan, Wed Jun 16 09:39:28 CST 2021
#*************************************************/
# * Redistributions in binary form must reproduce the above copyright
#   notice, this list of conditions and the following disclaimer in the
#   documentation and/or other materials provided with the distribution.
# * Neither the name of the axTLS project nor the names of its
#   contributors may be used to endorse or promote products derived
#   from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
# TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
# NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
# Generate the certificates and keys for testing.
#
#!/bin/sh
if [ "$1" != "source" ];then
  # source /usr/local/bin/redirect_shell_cmd
  function echo_dbg_p(){
    echo "echo_dbg, $@"
  }
  function usage(){
  echo -e "usages: $0 [-h|H|h|help] [-s]
    [-h|H|h|help]: check the usages
    [-s]: launch the script"
  }
  echo "YOU ARE IN DEBUG MODE OR DEV"
fi

#main
case $1 in
  source)
echo "#return: can only return from a function or sourced script"
    return 0
    ;;
  ""|H|h|help)
    usage
    ;;
  -*)
# getopts access the string after char "-"
    while getopts ":shc:" opt; do
      case $opt in
        h)
          usage
          ;;
        s)
          echo "$opt access"
          ;;
        c)
          echo "-s=$OPTARG"
          ;;
        :)
          echo "-$OPTARG needs an argument"
          ;;
        *)
          echo "$1 not recognized"
          ;;
      esac
    done
    ;;
  *)
    echo "$1 not support yet!"
    ;;
esac


if [ -n "$1" ] ;then
	encode_type=$1
else
	encode_type="sha1"
fi
if [ -n "$2" ] ;then
	PROJECT_NAME=$2
else
	PROJECT_NAME="TLS Project"
fi
certificate_path="certificate"$encode_type
#echo $certificate_path
# Generate the openssl configuration files.
cat > ca_cert.conf << EOF  
[ req ]
distinguished_name     = req_distinguished_name
prompt                 = no

[ req_distinguished_name ]
 O                      = $PROJECT_NAME Dodgy Certificate Authority
EOF

cat > server_cert.conf << EOF  
[ req ]
distinguished_name     = req_distinguished_name
prompt                 = no

[ req_distinguished_name ]
 O                      = $PROJECT_NAME
 CN                     = 192.168.199.188
EOF

cat > client_cert.conf << EOF  
[ req ]
distinguished_name     = req_distinguished_name
prompt                 = no

[ req_distinguished_name ]
 O                      = $PROJECT_NAME Device Certificate
 CN                     = 192.168.199.168
EOF
#mkdir -p $certificate_path/
mkdir -p $certificate_path/ca
mkdir -p $certificate_path/server
mkdir -p $certificate_path/client
mkdir -p $certificate_path/certDER
# private key generation
if [ "$encode_type" = "sha1" ];then
	openssl genrsa -out ca.key 1024
	openssl genrsa -out server.key 1024
	openssl genrsa -out client.key 1024
else
	openssl genrsa -$encode_type -out ca.key 1024
	openssl genrsa -$encode_type -out server.key 1024
	openssl genrsa -$encode_type -out client.key 1024
fi

# cert requests
openssl req -out ca.req -key ca.key -new \
            -config ./ca_cert.conf
openssl req -out server.req -key server.key -new \
            -config ./server_cert.conf 
openssl req -out client.req -key client.key -new \
            -config ./client_cert.conf 

# generate the actual certs.
if [ "$encode_type" = "sha1" ];then
	openssl x509 -req -in ca.req -out ca.crt -$encode_type -days 5000 -signkey ca.key
	openssl x509 -req -in server.req -out server.crt -$encode_type -CAcreateserial -days 5000 -CA ca.crt -CAkey ca.key
	openssl x509 -req -in client.req -out client.crt -$encode_type -CAcreateserial -days 5000 -CA ca.crt -CAkey ca.key
else
	openssl x509 -req -in ca.req -out ca.crt -days 5000 -signkey ca.key
	openssl x509 -req -in server.req -out server.crt -CAcreateserial -days 5000 -CA ca.crt -CAkey ca.key
	openssl x509 -req -in client.req -out client.crt -CAcreateserial -days 5000 -CA ca.crt -CAkey ca.key
#openssl x509 -req -in client.req -out client.crt -sha1 -CAcreateserial -days 5000 -CA ca.crt -CAkey ca.key
fi


openssl x509 -in ca.crt -outform DER -out ca.der
openssl x509 -in server.crt -outform DER -out server.der
openssl x509 -in client.crt -outform DER -out client.der


mv ca.crt ca.key $certificate_path/ca/
mv server.crt server.key $certificate_path/server/
mv client.crt client.key $certificate_path/client/

mv ca.der server.der client.der $certificate_path/certDER/

rm *.conf
rm *.req
rm *.srl

